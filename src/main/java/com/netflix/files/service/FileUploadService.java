package com.netflix.files.service;

import com.netflix.common.dto.Pagination;
import com.netflix.files.dto.FileUploadResponse;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface FileUploadService {
    Pagination getFileUploadByUserId(Long userId);

    FileUploadResponse uploadFile(MultipartFile multipartFile, Long userId) throws IOException;

    Resource downloadFile(String fileName, Long userId) throws IOException;
}
