package com.netflix.files.controller;

import com.netflix.files.dto.FileUploadResponse;
import com.netflix.files.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

@RestController
@RequestMapping(value = "/api/v1")
public class FileUploadController {
    @Autowired
    private FileUploadService fileUploadService;

    @PostMapping(value = "/users/{user-id}/upload")
    public ResponseEntity<FileUploadResponse> uploadFile(@RequestParam("file") MultipartFile multipartFile,
                                                         @PathVariable("user-id") String userId) throws IOException {
        FileUploadResponse fileUploadResponse = fileUploadService.uploadFile(multipartFile, Long.valueOf(userId));
        return ResponseEntity.status(HttpStatus.CREATED).body(fileUploadResponse);
    }

    @GetMapping(value = "/users/{user-id}/download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("fileName") String fileName,
                                                 @PathVariable("user-id") String userId) throws IOException {
        Resource resource = fileUploadService.downloadFile(fileName, Long.valueOf(userId));
        if (resource == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.ok(resource);
        }
    }
}
