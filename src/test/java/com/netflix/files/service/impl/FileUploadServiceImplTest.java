package com.netflix.files.service.impl;

import com.netflix.common.constants.Gender;
import com.netflix.common.customize.security.NetflixUserDetail;
import com.netflix.files.dto.FileUploadResponse;
import com.netflix.files.entities.FileUpload;
import com.netflix.files.entities.UserInformation;
import com.netflix.files.repository.FileUploadRepository;
import com.netflix.files.repository.UserInformationRepository;
import com.netflix.files.utils.FileStorageLocationUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.*;

public class FileUploadServiceImplTest {
    private MultipartFile multipartFile;
    @Mock
    private FileUploadRepository fileUploadRepository;
    @Mock
    private UserInformationRepository userInformationRepository;
    @Mock
    private FileStorageLocationUtil fileStorageLocationUtil;
    @InjectMocks
    private FileUploadServiceImpl fileUploadService;

    @Before
    public void setUp() throws Exception {
        NetflixUserDetail netflixUserDetail = new NetflixUserDetail("1", "requestBody");
        SecurityContextHolder.getContext().setAuthentication(netflixUserDetail);
        this.multipartFile = new MockMultipartFile("patient-information.csv", "patient-information.csv", "text/csv", new byte[]{1, 2, 3, 4, 5, 6});
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void uploadFile() throws IOException {
        UserInformation userInformation = new UserInformation();
        userInformation.setUserId(1L);
        userInformation.setUsername("userName");
        userInformation.setPassword("password");
        userInformation.setDateOfBirth(LocalDate.now());
        userInformation.setGender(Gender.MALE);
        when(userInformationRepository.findUserInformationByUserId(anyLong(), anyBoolean())).thenReturn(Optional.of(userInformation));
        when(fileUploadRepository.getFileUploadByFileName(anyString(), anyBoolean())).thenReturn(Collections.emptyList());
    }

    @Test
    public void downloadFile() {
    }
}